import config
import telebot
from datetime import date
import json
import random


bot = telebot.TeleBot(config.token)
bot_text = 'Happy Birthday, Nikol!!!'
file = open('wishes.json')
text_wishes = json.load(file)

file = open('audio.json')
audio_wishes = json.load(file)

# print(random.choice(wishes['wishes'])['text'])

@bot.message_handler(commands=['start'])
def send_welcome(message):
    today = date.today().strftime("%d.%m")
    if config.chatId == message.chat.id:
        bot.reply_to(message, "Hi Nikol!")
    else:
        bot.reply_to(message, "This chat is for Nikol ONLY!")
    if today == config.birthdayDate and config.chatId == message.chat.id:
        bot.reply_to(message, "Happy Birthday, Nikol!!!")


@bot.message_handler(commands=['love_options'])
def love_options(message):
        # bot.reply_to(message, "Love options!!!")
        button_text_wishes = telebot.types.InlineKeyboardButton('Text wishes', callback_data='text_wishes')
        button_audio = telebot.types.InlineKeyboardButton('Audio', callback_data='audio')
        button_photo = telebot.types.InlineKeyboardButton('Photo', callback_data='photo')
        button_video = telebot.types.InlineKeyboardButton('Video', callback_data='video')

        keyboard = telebot.types.InlineKeyboardMarkup()
        keyboard.add(button_text_wishes)
        keyboard.add(button_audio)
        # keyboard.add(button_photo)
        # keyboard.add(button_video)

        bot.send_message(config.chatId, text='Your love options:', reply_markup=keyboard)



@bot.callback_query_handler(func=lambda call: True)
def callback_query(message):
    if message.data == "text_wishes":
        bot.send_message(config.chatId, random.choice(text_wishes['wishes'])['text'])
    elif message.data == "photo":
        bot.send_message(config.chatId, "Some photos")
    elif message.data == "video":
        bot.send_message(config.chatId, "Some videos")
    elif message.data == "audio":
        bot.send_audio(config.chatId, audio=open(random.choice(audio_wishes['audio-wishes'])['file'], 'rb'))
    else:
        bot.send_message(config.chatId, "Uknown")
bot.infinity_polling()



